from ambulance import Ambulance
import googlemaps

gmaps = googlemaps.Client(key='AIzaSyDJACrpPh2q-WKqPUtf_zTfiPazbKiey1Q')


def nearest_ambulance(inp):
    a = Ambulance()
    code = gmaps.geocode(inp)
    coordinates = code[0]['geometry']['location']
    return a.get_nearest_ambulance(coordinates['lat'], coordinates['lng'])

if __name__ == '__main__':
    print(nearest_ambulance('vidyanagar'))
