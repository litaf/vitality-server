
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

url = "/home/sharoon/vtals.csv"
names = ['map_1st',	'hr_1st' , 'temp_1st' ,	'spo2_1st' , 'chf_flg' , 'afib_flg' , 'cad_flg' , 'stroke_flg']
dataset = pandas.read_csv(url, names=names)

print(dataset)
