from tornado import websocket, web, ioloop
import json
from vital import Vital
from mine import Mine
import time
import serial

ser = serial.Serial()
ser.baudrate = 115200
ser.port = '/dev/ttyACM0'
# l = []
ser.open()

vital = Vital()
mine = Mine()
# MOCK_DATA = False

class IndexHandler(web.RequestHandler):
	def get(self):
		self.render("index.html")

class MockHandler(web.RequestHandler):
	def get(self):
		self.render("mock.html")

class DriverHandler(web.RequestHandler):
	def get(self):
		self.render("driver.html")

class HospitalHandler(web.RequestHandler):
	def get(self):
		self.render("hospital.html")

class MockDataHandler(web.RequestHandler):
	def post(self):
		vital.heart_rate = self.get_argument('heart-rate')
		# print(vital.heart_rate)

		vital.temperature = self.get_argument('temperature')
		# print(vital.temperature)

		vital.resp_rate = self.get_argument('resp-rate')
		# print(vital.resp_rate)

		vital.bp = self.get_argument('blood-pressure')
		# print(vital.bp)

		self.redirect("/mock")

class SocketHandler(websocket.WebSocketHandler):
	#global MOCK_DATA
	#MOCK_DATA = False
	def open(self):
		print("Connection - Opened")
		# while True:
		# 	if(MOCK_DATA == True):
		# 		print(vital.heart_rate)
		# 		print(vital.temperature)
		# 		print(vital.resp_rate)
		# 		print(vital.bp)
		# 	else:
		while True:
			print("T : " + ser.readline().decode().strip().split(":")[0])
			print("H : " + ser.readline().decode().strip().split(":")[1])
			print("R : " + vital.resp_rate)
			print("B : " + vital.bp)
			print("---------")
			time.sleep(1)

		# while True:
		# 	times = 12
		# 	while times > 0:
		# 		while True:
		# 			# read values every 5 seconds
		# 			print("inner")
		# 			print(ser.readline().decode().strip().split(":"))
		# 			time.sleep(5)
		# 			# take value
		# 			break
		# 		print("outer")
		# 		times = times - 1
		# 		# compute average after a minute
		# 	print("outer outer")
			# mine the data abd get the result


			# v_list = ser.readline().decode().strip().split(":")
			# l.append("16")
			# l.append("100/70")
			# self.write_message(str(l))
			# mine.get_class_label(v_list[0], v_list[1], vital.resp_rate, vital.bp)
		# mine data here
		# give the class label and send back to the client

	def on_message(self, msg):
		print(msg)
		if msg == 'MOCKON':
			# MOCK_DATA = True
			# print(MOCK_DATA)
			print(vital.heart_rate)
			print(vital.temperature)
			print(vital.resp_rate)
			print(vital.bp)
		else:
			# MOCK_DATA = False
			# print(MOCK_DATA)
			print(ser.readline().decode().strip().split(":"))
			print(vital.resp_rate)
			print(vital.bp)


	def on_close(self):
		pass


app = web.Application([
	(r'/', IndexHandler),
	(r'/ws', SocketHandler),
	(r'/m', MockDataHandler),
	(r'/mock', MockHandler),
	(r'/drive', DriverHandler),
	(r'/hospital', HospitalHandler),
], template_path='templates', static_path='static', debug=True)

if __name__ == '__main__':
	print("Server Started at http://localhost:8888")
	app.listen(8888)
	ioloop.IOLoop.instance().start()
