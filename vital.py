class Vital(object):
	def __init__(self):
		self._heart_rate = None
		self._temperature = None
		self._bp = None
		self._resp_rate = None
		
	@property
	def heart_rate(self):
		return self._heart_rate

	@heart_rate.setter
	def heart_rate(self, value):
		self._heart_rate = value

	@property
	def temperature(self):
		return self._temperature

	@temperature.setter
	def temperature(self, value):
		self._temperature = value

	@property
	def bp(self):
		return self._bp

	@bp.setter
	def bp(self, value):
		self._bp = value

	@property
	def resp_rate(self):
		return self._resp_rate

	@resp_rate.setter
	def resp_rate(self, value):
		self._resp_rate = value