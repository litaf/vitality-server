var ws = new WebSocket("ws://localhost:8888/ws");
function WebSocketTest() {
  if ("WebSocket" in window) {
     console.log("WebSocket is supported by your Browser!");
     // Let us open a web socket
   
     ws.onopen = function() {
          console.log("Connection - Open");
          ws.send("abcdefg");
     };

     ws.onmessage = function (evt) {
      console.log(evt.data);
     };

     ws.onclose = function() {
      console.log("Connection - Closed");
     };
     window.onbeforeunload = function(event) {
        socket.close();
     };
  }
  else {
     console.log("WebSocket NOT supported by your Browser!");
  }
}

var heartrate = document.getElementById("heart-rate");
var bp = document.getElementById("blood-pressure");
var resp = document.getElementById("resp-rate");
var temp = document.getElementById("temperature");

var heartrateValue = document.getElementById("heart-rate-value");
var bpValue = document.getElementById("blood-pressure-value");
var respValue = document.getElementById("resp-rate-value");
var tempValue = document.getElementById("temperature-value");

heartrateValue.innerHTML = heartrate.value + " BPM";
bpValue.innerHTML = bp.value + " mHg";
respValue.innerHTML = resp.value + " breaths/min";
tempValue.innerHTML = temp.value + " *C";


heartrate.oninput = function() {
  heartrateValue.innerHTML = this.value + " BPM";
}

bp.oninput = function() {
  bpValue.innerHTML = this.value + " mmHg";
}

temp.oninput = function() {
  tempValue.innerHTML = this.value + " *C";
}

resp.oninput = function() {
  respValue.innerHTML = this.value + " breaths/min";
}

function mockON() {
  var mockBox = document.getElementById("switch-mock");
  if (mockBox.checked == true){
    console.log("checked")
    ws.send("MOCKON")
    heartrate.disabled = false;
    temp.disabled = false;
  } else {
    console.log("not checked")
    ws.send("MOCKOFF")
    heartrate.disabled = true;
    temp.disabled = true;
  }
}