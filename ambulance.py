import googlemaps
import pprint
from datetime import datetime
from pymongo import MongoClient
from operator import itemgetter

class Ambulance(object):
    def __init__(self):
        self.gmaps = googlemaps.Client(key='AIzaSyDJACrpPh2q-WKqPUtf_zTfiPazbKiey1Q')
        self.client = MongoClient('localhost', 27017)

    def get_nearest_ambulance(self, lat, lon):
        db = self.client.vitality
        node = dict()
        for n in db.ambulance.find({'status': 0}):
            dist = self.gmaps.distance_matrix((str(lat) + ", " + str(lon)), 
                (str(n['lat']) + ", " + str(n['lon'])), mode = 'driving')
            node[n['id']] = dist['rows'][0]['elements'][0]['distance']['value']
        min_key, _ = min(node.iteritems(), key=itemgetter(1))
        return min_key  

if __name__ == '__main__':
    a = Ambulance()
    print a.get_nearest_ambulance(26.8802042, 75.8127309)


# FRIENDLY NAME
# vitality
# SID
# SKcb6167b5a05df3d0ae66e4a6ef94fd42
# KEY TYPE
# Standard
# SECRET
# J9ep2Lz76WjfxvyLqkI5MlU5WVHFDx3E