27% percent of total deaths in india is due to lack of medical attention during trauma, our solution aims to solve this problem.

Our Solution 'Vitality' is a low cost, portable and smart setup which aids the emergency response team i.e Ambulance. It is a full-blown setup which aids the emergency response team from the location of the trauma till the victim is admitted to the hospital.

A first responder calls a dedicated number, and tells the location of trauma, using Speech to text, the location is found out and the nearest ambulance is a sent an alert, when the ambulance finally arrives at the location, two of our sensors(temperature sensor module, camera and high lumen LED setup) are plugged to the victim, now this will stream the vital signs i.e blood pressure, respiration rate, heart rate, oxygen saturation and temperature, to our server, the system intelligently selects a hospital based on the vital signs using SVM data mining which is suitable for this application, the selection is done based on the availability of the doctors of a particular symptom i.e cardiologist, and the inventory available in the hospital i.e oxygen, blood supply, emergency wards. These factors are all considered such that patient life is not put to risk.

Traffic is one of the biggest woes in the emergency response mechanism, we propose a solution wherein the traffic lights is controlled by our system which gives priority to us system, by opening and closing the signals preemptively, so that the ambulance is not faced with any delay.

The fact that we can stream the data to the hospital, and the fact that we can actually predicted the symptoms from which victim be it maguffering, can leverage the process of preparing for treatment of the victim, so that the hospital can make necessary arrangements, acquire doctors etc. 

Our solution ensures that the victim is not put do danger with hospital mismanagement, it is able to take smart decisions taking the patient's vitals and traffic into consideration.


	In our system victim is taken from trauma locaton to hospital with the help of ambulance where the vital signs are calculated and predict the symptoms from which victim is suffering, using the concept of data mining, before reaching to the hospital. And we considered the traffic problem, by controlling the traffic lights and providing the priority to our system we can solve this problem also.


In our system victim is taken from trauma locaton to hospital with the help of ambulance where the vital signs are calculated and predict the symptoms from which victim is suffering, using the concept of data mining, before reaching to the hospital.
